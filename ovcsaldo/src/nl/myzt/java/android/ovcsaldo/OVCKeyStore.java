package nl.myzt.java.android.ovcsaldo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.myzt.ovchipcard.OVCParser;

import android.os.Environment;
import android.util.Log;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class OVCKeyStore {
	
	private final static String KEYSTORE_DIR="nl.myzt.ovsaldo" + File.separator	+ "keys";
	private final static String KEYSTORE_EXT=".tsv";
	private final static int KEY_A= 1;
	private final static int KEY_B= 2;
	private static Map<String,OVCKeyStore> cardlist = new HashMap<String,OVCKeyStore>();
	
	private List<String[]> keys = null;
	
	private static File getKeyStoreDir(){
		File dir_extstore = Environment.getExternalStorageDirectory();
		File mydir = new File(dir_extstore, KEYSTORE_DIR);
		return mydir;
	}

	private static void createKeyStoreDir(){
		File mydir = getKeyStoreDir();
		if (!mydir.exists()){
			mydir.mkdirs();
		}
		
	}
	public static boolean keyStoreExists(String cardID) {
		File dir_extstore = Environment.getExternalStorageDirectory();
		File keyfile = new File(dir_extstore, KEYSTORE_DIR + File.separator + cardID + KEYSTORE_EXT);
		if (keyfile.exists()){
			return true; 
		}
		return false;
	}
	
	public static OVCKeyStore createKeyStore(String cardID, byte [] cardDump){
		if (!cardlist.containsKey(cardID)){
			createKeyStoreDir();
			File dir_keystore = getKeyStoreDir();
			File file = new File(dir_keystore, "" + cardID + KEYSTORE_EXT);
			try{
				CSVWriter writer = new CSVWriter (new FileWriter(file), '\t');
				OVCParser ovcp = new OVCParser(cardDump);
				writer.writeAll(ovcp.getKeys());
				writer.close();
			}catch (IOException e) {
				Log.w("OVCKeyStore.readKeyFile", "Cannot create keystore:" + file, e);
			}
			cardlist.put(cardID, new OVCKeyStore(cardID));
		}
		return cardlist.get(cardID);		
	}
	
	public static OVCKeyStore openKeyStore(String CardID){
		if (!cardlist.containsKey(CardID) && keyStoreExists(CardID)){
			cardlist.put(CardID, new OVCKeyStore(CardID));
		}
		return cardlist.get(CardID);
	}
	
	private OVCKeyStore (String CardID){
		this.keys=this.readKeyFile(CardID);
	}
	
	private List<String[]> readKeyFile(String cardID) {
		File dir_keystore = getKeyStoreDir();
		File file = new File(dir_keystore, "" + cardID + KEYSTORE_EXT);
		List<String[]> l = new ArrayList<String[]>();
		try {
			CSVReader reader = new CSVReader(new FileReader(file), '\t');
			l = reader.readAll();
			return l;
		} catch (IOException e) {
			Log.w("OVCKeyStore.readKeyFile", "Cannot read file:" + file, e);
		}
		return null;
	}
	
	private String getKey(int sector, int key){
		if (sector < this.keys.size()){
			String [] row = this.keys.get(sector);
			if (row.length == 3){
				switch (key){
				case 2:
					return this.keys.get(sector)[2];
				default:
					return this.keys.get(sector)[1];
				}
			}
		}
		return null;
	}
	
	public String getKeyA(int sector){
		return this.getKey(sector,KEY_A);
	}

	public String getKeyB(int sector){
		return this.getKey(sector,KEY_B);
	}

	
}
