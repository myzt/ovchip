package nl.myzt.java.android.ovcsaldo;
import java.util.ArrayList;
import java.util.List;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import nl.myzt.ovchipcard.OVCParser;
import nl.myzt.ovchipcard.OVCTransaction;






public class TransactionFragment extends ListFragment {
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    	OVChipData act = (OVChipData) this.getActivity();
    	OVCParser ovc_parser = act.getOVCParser(); 
        List <OVCTransaction> all = new ArrayList<OVCTransaction>();
        if(ovc_parser.getSaldo() != ""){
        	all.addAll(ovc_parser.getTransactionHistory());
        	all.addAll(ovc_parser.getChargeHistory());      //also makes a nice separator
        	all.addAll(ovc_parser.getTravelHistory());
        }
        setListAdapter(new TransactionAdapter(getActivity(),
                android.R.layout.simple_list_item_1, all));
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }
    


}
