package nl.myzt.java.android.ovcsaldo;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import nl.myzt.java.android.ovcsaldo.R;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import nl.myzt.java.android.ovcsaldo.OVCReader;


public class OVChipActivity extends Activity {
	TextView debug = null;
	byte [] carddata = null;
	ProgressDialog dialog = null;
	

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	this.debug = (TextView) findViewById(R.id.debugText);
    
    	super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
    	this.debug = (TextView) findViewById(R.id.debugText);

    	Button btn_open = (Button) findViewById(R.id.btn_open_dump);
        
        btn_open.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View arg0) {
        	    Intent intent = new Intent("org.openintents.action.PICK_FILE");
        	    intent.setData(Uri.parse("file:///sdcard/nl.myzt.ovsaldo/dumps/"));
        	    startActivityIfNeeded(intent, 1);
          }
        });
        
        Intent i=this.getIntent();
        if (i.hasExtra("ReadCard")  && i.getExtras().getBoolean("ReadCard")){
       	 this.dialog = ProgressDialog.show(this, "", 
                 "Reading data. Please do not remove card...", true);
        	Tag tag = (Tag) getIntent().getExtras().getParcelable(NfcAdapter.EXTRA_TAG);
			MifareClassic mfc = MifareClassic.get(tag);
		    new ReadOVC().execute(mfc);
        }
        
    }    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	byte[] dump = null;
		if (requestCode == 1) { // read dump
			try {
				dump = readData(URI.create(data.getData().toString()));
				Intent i = new Intent();
			    i.setClass(this, OVChipData.class);
			    i.putExtra("dump", dump);
			   // startActivity(i);
			    startActivityIfNeeded(i,1);
			} catch (Exception e) {
				e.printStackTrace();
			}	    

		}

	}
	public void dismissDialog(){
		this.dialog.dismiss();
	}
    
    @Override
	public void onResume() {
       super.onResume();
       if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())){
		    Intent intent=new Intent(OVChipActivity.this,OVChipActivity.class);
		    intent.putExtra("ReadCard", true);
		    PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		    NfcAdapter adapter =  NfcAdapter.getDefaultAdapter(OVChipActivity.this);
		    adapter.enableForegroundDispatch(OVChipActivity.this, pi, null, null);
         }
       

	}

  	public static byte[] readData(URI filename) {
		try {
			File file = new File(filename);
			FileInputStream dumpis = new FileInputStream(file);
			int size=(int)file.length();
			byte[] data = new byte[size];
			dumpis.read(data, 0, size);
			dumpis.close();
			return data;
		} catch (FileNotFoundException fe) {
			System.out.println(fe);
		} catch (IOException fe) {
			System.out.println(fe);
		}
		return null;
	}
  	
  	private class ReadOVC extends AsyncTask<MifareClassic,Void,byte []> {
  
  		protected byte [] doInBackground(MifareClassic... mfc) {
  	    	
  			
  			try{
  				OVCReader ovc = new OVCReader(mfc[0]);
	  	    	return ovc.readCard();
  			}catch (IOException e){
  				e.printStackTrace();
  			}
  			return null;
  		}
  	
  	    protected void onPostExecute(byte [] result) {
			carddata = result;
			Intent i = new Intent();
		    i.setClass(OVChipActivity.this, OVChipData.class);
		    i.putExtra("dump", carddata);
		    startActivity(i);
		    OVChipActivity.this.dialog.dismiss();
  	    }
  	}
}

