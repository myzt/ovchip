package nl.myzt.java.android.ovcsaldo;

import java.util.List;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import nl.myzt.ovchipcard.OVCParser;
import nl.myzt.ovchipcard.OVCSubscription;


public class SubscriptionFragment extends ListFragment {
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
    	OVChipData act = (OVChipData) this.getActivity();
    	OVCParser ovc_parser = act.getOVCParser(); 
        List <OVCSubscription>  meuk = ovc_parser.getSubscriptions();
        
        setListAdapter(new ArrayAdapter<OVCSubscription>(getActivity(),
                android.R.layout.simple_list_item_1, meuk));
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }

}
