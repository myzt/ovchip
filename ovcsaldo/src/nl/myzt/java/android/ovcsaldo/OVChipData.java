package nl.myzt.java.android.ovcsaldo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;

import nl.myzt.java.android.ovcsaldo.R;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.content.Intent;
import nl.myzt.ovchipcard.OVCParser;


public class OVChipData extends Activity {
	private OVCParser ovc = null;
	private byte [] carddata= null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.carddata = getIntent().getByteArrayExtra("dump");
        if (this.carddata == null || this.carddata.length!=4096){
        	this.carddata= new byte [4096];
        }
        	this.ovc = new OVCParser(this.carddata);
        	
        if(this.ovc.getSaldo() != ""){ //Can't read sector 39 == no keys;
	        if (!OVCKeyStore.keyStoreExists(ovc.getID())){
	        	OVCKeyStore.createKeyStore(ovc.getID(), carddata);
	        }
        }
        this.ovc.parse();
        
        showTabsNav();
        setContentView(R.layout.show_data);        
    }
    
    public OVCParser getOVCParser(){
    	return this.ovc;
    }
    
    private void showTabsNav() {
        ActionBar ab = getActionBar();
        if (ab.getNavigationMode() != ActionBar.NAVIGATION_MODE_TABS) {
                ab.setDisplayShowTitleEnabled(true);
                ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
                ab.setTitle("OV Chipkaart dump reader");
                
                InfoFragment f_info=new InfoFragment();
                Fragment f_subscriptions=new SubscriptionFragment();
                Fragment f_transactions=new TransactionFragment();
                //f_info.changeData(this.dumpdata);
                Tab tab1 = ab.newTab().setText(R.string.info);
                Tab tab2 = ab.newTab().setText(R.string.transactions);
                Tab tab3 = ab.newTab().setText(R.string.subscriptions);

                tab1.setTabListener(new TabListener((Fragment) f_info));
                tab2.setTabListener(new TabListener(f_transactions));
                tab3.setTabListener(new TabListener(f_subscriptions));

                
                ab.addTab(tab1);
                ab.addTab(tab2);
                ab.addTab(tab3);
                
                ab.show();
                
        }
}
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) { // write dump
			try {
				System.out.println("wajooo");
				URI filename = URI.create(data.getData().toString());
				System.out.println(filename);
				writeDump(filename);
			} catch (Exception e) {
				e.printStackTrace();
			}	    

		}

	}
    private void writeDump(URI uri){
    	File file = new File(uri);
    	try {
    		FileOutputStream f = new FileOutputStream(file);
			f.write(this.carddata);
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    }
    
}


