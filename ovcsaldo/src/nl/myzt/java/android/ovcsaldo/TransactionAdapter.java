package nl.myzt.java.android.ovcsaldo;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.List;

import nl.myzt.ovchipcard.OVCTransaction;
import nl.myzt.ovchipcard.OVCUtil;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TransactionAdapter extends ArrayAdapter<OVCTransaction> {
	private List<OVCTransaction> items;
	private SQLiteDatabase stationdb=null;

	public TransactionAdapter(Context context, int textViewResourceId, List<OVCTransaction> transactions) {
		super(context, textViewResourceId, transactions);
		// TODO Auto-generated constructor stub
		this.items=transactions;
        this.stationdb=SQLiteDatabase.openDatabase("/sdcard/nl.myzt.ovsaldo/stations/stations.sqlite",null,SQLiteDatabase.OPEN_READONLY);

	}
		
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    
            View v = convertView;

            if (v == null) {
            	LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.transactionlist_row, null);
            }
            final OVCTransaction t = items.get(position);
            
    	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    	    NumberFormat nf = NumberFormat.getCurrencyInstance();
    	    nf.setCurrency(Currency.getInstance("EUR"));
    	    if (OVCUtil.COMPANY.get(t.getField(OVCTransaction.T_COMPANY)) == null){
    	    	return v;
    	    }
    	    
    	    String toptext="" + df.format(OVCUtil.toDate(t.getField(OVCTransaction.T_DAYS), 
    	    											 t.getField(OVCTransaction.T_MINUTES)
    	    					                        )
    	    					         ) +
    	    			   
    	    			   " " + OVCUtil.COMPANY.get(t.getField(OVCTransaction.T_COMPANY));
    	    
    	    String amounttext="" + nf.format((double)t.getField(OVCTransaction.T_AMOUNT)/100);
    	    
    	    
    	    String bottomtext= "";
    	    
    	    int station = t.getField(OVCTransaction.T_STATION);
    	    if (station != -1) {
               Cursor cursor = this.stationdb.query("stations_data", 
                		new String [] {"name", "city", "lon", "lat" },
                		"ovcid=? and company=?",
                		new String [] {Integer.toString(t.getField(OVCTransaction.T_STATION)),Integer.toString(t.getField(OVCTransaction.T_COMPANY))},
                		null,
                		null,
                		null);
                if (cursor.getCount()> 0) {
	                cursor.moveToFirst();
	    	    	bottomtext += "" + cursor.getString(0);
	    	    	if (cursor.getString(1) != null) {
	    	    		bottomtext += ", " + cursor.getString(1);
	    	    	}
	    	    	bottomtext += " ";
                	final String lat = cursor.getString(3);
                	final String lon = cursor.getString(2);   	    	
            		ImageView iv_loc = (ImageView) v.findViewById(R.id.iv_location );     		
                	iv_loc.setColorFilter(0xffffffff);
    	            iv_loc.setVisibility(ImageView.INVISIBLE);

	    	    	if (lon != null && lat != null){
	    	            iv_loc.setVisibility(ImageView.VISIBLE);
	                	final String loc = bottomtext;
	    	            v.setOnClickListener(new View.OnClickListener() {
	    	                public void onClick(View v) {
	    	                	String uri = "geo:"+ lat + "," + lon + "?q=" + lat + "," + lon + "(" + loc + ")";
	    	                	Intent igeo = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
	    	                	TransactionAdapter.this.getContext().startActivity(igeo);
	    	                }
	    	            });
	    	    	}else{
	    	            v.setOnClickListener(null);
	    	    	}
                }else{
                	bottomtext += "s: " + station + " ";
                }
    	    }
    	    		
    	    	   bottomtext += "m: " + t.getField(OVCTransaction.T_MACHINE) +  " " +
  			       			  "vh:" + t.getField(OVCTransaction.T_VEHICLE) + " " +
    	    			      //"s: " + t.getField(OVCTransaction.T_SUBSCRIPTION) +  " " +
    	    			      "   ";
    	    
            if (t != null) {
            		ImageView iv = (ImageView) v.findViewById(R.id.icon);
            		if (t.getAction() == 1){     // Checkin        		
            			iv.setImageResource(R.drawable.arrow_up);
            			iv.setRotation(-90);
            			iv.setColorFilter(0x99ff0000);
            			
            		} else if (t.getAction() == 2){ //Checkout
            			iv.setImageResource(R.drawable.arrow_up);
            			iv.setRotation(90);
            			iv.setColorFilter(0x9900ff00);
            			
            		} else if (t.getAction() == 6){ //Overstap
            			iv.setImageResource(R.drawable.arrow_up);
            			iv.setRotation(0);

            			iv.setColorFilter(0x00000000);

            			iv.setRotation(180);
            		} else if (t.getAction() == -1){
            			iv.setColorFilter(0x00000000);
            			iv.setRotation(0);
            			iv.setImageResource(R.drawable.ic_launcher);

            		}
            		
                    TextView at = (TextView) v.findViewById(R.id.amounttext);

                    TextView tt = (TextView) v.findViewById(R.id.toptext);
                    TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                    if (at != null) {
                        at.setText(amounttext);
                    }
                    if (tt != null) {
                          tt.setText(toptext);
                    }
                    if(bt != null){
                          bt.setText(bottomtext);
                    }
            }
            

            return v;
    }
}
