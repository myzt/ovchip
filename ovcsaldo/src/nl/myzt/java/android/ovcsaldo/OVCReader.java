package nl.myzt.java.android.ovcsaldo;
import android.nfc.tech.MifareClassic;
import android.util.Log;
import java.io.IOException;

import nl.myzt.ovchipcard.OVCParser;


public class OVCReader{
	
	private MifareClassic mfc = null;
	
	public OVCReader(MifareClassic mfc) throws IOException{
		this.mfc = mfc; 
        try {
			this.mfc.connect();
		} catch (IOException e) {
			Log.w("OVCReader()", "Cannot connect to tag.");
		}
	}
	
    public byte [] readCard () throws IOException{

		if (!OVCKeyStore.keyStoreExists(getCardID())) {
			byte[] data = new byte[4096];
			byte[] buf = this.readSector(0, "000000000000", "b5ff67cba951");
			System.arraycopy(buf, 0, data, 0, buf.length);
			return data;
		}
    	
    	OVCKeyStore keys=OVCKeyStore.openKeyStore(getCardID());
    	byte [] data = readCard(keys);
    	this.mfc.close();
    	return data;
    }
    
    
    public String getCardID () {
    	try {
    		byte [] first = readSector(0, "000000000000","b5ff67cba951");
    		OVCParser ovcp = new OVCParser(first);
    		return ovcp.getID();
    	}catch (Exception e){
    		Log.w("OVCReader.getCardID", "Can't read public card data");
    	}
    	return null;
    }
    
    public byte [] readCard (OVCKeyStore keys) throws IOException{
    	byte[] data = new byte [this.mfc.getSize()];
        int offset =0;
        for (int i=0; i < this.mfc.getSectorCount(); i++) {
            	byte [] sect = readSector(i, keys.getKeyA(i), keys.getKeyB(i));   
            	System.arraycopy(sect, 0, data, offset, sect.length);
              	offset += sect.length;
        }
        return data;
    }
    
    public byte [] readSector(int sect, String keyA, String keyB) throws IOException {
			if (keyA != null) {
				if(!this.mfc.authenticateSectorWithKeyA(sect,
						OVCReader.HexStringToByteArray(keyA))){
					Log.w("OVCReader.readSector", "Authentication failed for keyA: " + keyA + " on sect " + sect);
				}
			}
			if (keyB != null) {
				if(!this.mfc.authenticateSectorWithKeyB(sect,
						OVCReader.HexStringToByteArray(keyB))){
					Log.w("OVCReader.readSector", "Authentication failed for keyB: " + keyB + " on sect " + sect);
			}
			}
	    	if (!this.mfc.isConnected()){
	    		this.mfc.connect();
	    	}
			int blocks = this.mfc.getBlockCountInSector(sect);
			byte[] data = new byte[blocks * 16];
			for (int i = 0; i < blocks; i++) {
				try {
				System.arraycopy(
						this.mfc.readBlock(this.mfc.sectorToBlock(sect) + i),
						0, data, i * 16, 16);
				}catch (IOException e){
					Log.w("OVCReader.readSector", "Can't read block " + (this.mfc.sectorToBlock(sect) + i) + " in sector: " + sect);
				}
			}
			return data;			
    }

    public static byte[] HexStringToByteArray(String s) {
        byte data[] = new byte[s.length()/2];
        for(int i=0;i < s.length();i+=2) {
            data[i/2] = ((Integer.decode("0x"+s.charAt(i)+s.charAt(i+1)))).byteValue();
        }
        return data;
    }
    
}
