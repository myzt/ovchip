package nl.myzt.ovchipcard;

import java.util.ArrayList;
import java.util.List;


public class OVCTransactionIndex {
	public static List<OVCTransaction> getTransactionHistory(byte [] carddata){
		List<OVCTransaction> l = new ArrayList<OVCTransaction>();
		List<Integer> index = readIndex(carddata,156, 10);
		for (int i=0; i < index.size(); i++) {
			//System.out.println(index.get(i));
			l.add(OVCTransaction.getTransaction(carddata, index.get(i)));
		}
		return  l;
	}
	
	public static List<OVCTransaction> getTravelHistory(byte [] carddata){
		List<OVCTransaction> l = new ArrayList<OVCTransaction>();
		List<Integer> index = readIndex(carddata, 196, 13);
		for (int i=0; i< index.size(); i++) {
			l.add(OVCTransaction.getTransaction(carddata, 11 + index.get(i)));
		}
		return  l;
	}
	
	
	public static List<OVCTransaction> getChargeHistory(byte [] carddata){
		byte [] buf = new byte[32];
		System.arraycopy(carddata, 0xfb0, buf, 0, 32);			
		List<OVCTransaction> l = new ArrayList<OVCTransaction>();
		int num = (int) OVCUtil.getBits(244, 4, buf);
		//num=1;
		if (num == 1 ){
			l.add(OVCTransaction.getTransaction(carddata, 25));
			l.add(OVCTransaction.getTransaction(carddata, 26));
			l.add(OVCTransaction.getTransaction(carddata, 27));
		}else if (num == 6){
			l.add(OVCTransaction.getTransaction(carddata, 26));
			l.add(OVCTransaction.getTransaction(carddata, 27));
			l.add(OVCTransaction.getTransaction(carddata, 25));
		}else if (num == 8){
			l.add(OVCTransaction.getTransaction(carddata, 27));
			l.add(OVCTransaction.getTransaction(carddata, 25));
			l.add(OVCTransaction.getTransaction(carddata, 26));	
		}
		return  l;
	}
	
	
	private static List<Integer> readIndex(byte [] carddata, int offset, int size) {
		byte [] buf = new byte[32];
		System.arraycopy(carddata, 0xfb0, buf, 0, 32);	
		List<Integer> l = new ArrayList<Integer>();
		for (int i=0; i < size; i++){
			l.add((int) OVCUtil.getBits(offset+(4*i), 4, buf));
		}
		return  l;
	}

}
/*

UNUSED CRAP  TODO: 

public void transIdx() {
	byte [] buf = new byte[32];
	System.arraycopy(this.carddata, 0xfb0, buf, 0, 32);	
	int id = (int) OVCUtil.getBits(10, 16, buf);
	int ki = (int) OVCUtil.getBits(26, 1, buf);
//	long all = (long) OVCUtil.getBits(0, 32, buf);
//	long idx1 = OVCUtil.getBits(32, 28, buf);
	System.out.println("---");
	for (int i=0; i<15; i++){
		System.out.println("asi["+Integer.toHexString(i)+"]:" + (int)OVCUtil.getBits(60+(4*i), 4, buf));
		
	}
	System.out.println("---");
	for (int i=0; i<12; i++){
		System.out.println("ai["+Integer.toHexString(i)+"]:" + this.subscriptions.get((int)OVCUtil.getBits(108+(4*i), 4, buf)));
		
	}

	System.out.println("op:" + Long.toHexString(OVCUtil.getBits(244, 4, buf)));

	System.out.println("id: " + id + "ki: " + ki + " " );
}
*/