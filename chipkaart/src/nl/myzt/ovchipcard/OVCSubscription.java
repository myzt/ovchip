package nl.myzt.ovchipcard;

public class OVCSubscription{
	private long identifier=-1;
	private long company=-1;
	private long type=-1;
	byte [] buf = new byte[16];
	
	public OVCSubscription(byte [] data){
		//206 	0x0ce 	4 (NS) Voordeeluurtjes
		System.arraycopy(data, 0, this.buf, 0, 16);
		this.identifier = OVCUtil.getBits(0,28,this.buf);
		this.company = OVCUtil.getBits(32,4,this.buf);
		this.type = OVCUtil.getBits(36,16,this.buf);
	}
	
	public boolean isEmpty(){
		if (identifier == 0){
			return true;
		}
		return false;
	}
	
	public String getID() {
		return Long.toHexString(this.identifier); 
	}
	
	public String getType() {
		return Long.toHexString(this.type); 
	}
	
	public String toString(){
		if (identifier == 0 ){
			return "empty";
		}
		return OVCUtil.COMPANY.get((int)this.company) + " type: "+ OVCUtil.SUBSCRIPTION.get((int)this.type) + " (" + Long.toHexString(this.identifier) + ")";
	}
}